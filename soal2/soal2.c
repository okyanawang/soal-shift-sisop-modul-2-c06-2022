#include <sys/types.h>
#include <sys/stat.h>
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <errno.h>
#include <unistd.h>
#include <syslog.h>
#include <string.h>
#include <dirent.h>
#include <signal.h>
#include <wait.h>
#include <stdbool.h>

void create_folder(pid_t pid){
    int status;

    if (pid == 0) {
        char *argv[] = {"mkdir", "-p", "/home/aaliyah/shift2/drakor", NULL};
        execv("/bin/mkdir", argv);
    }else{
        while((wait(&status)) > 0);
        char *argv[] = {"unzip", "/home/aaliyah/sisop/modul2-soal2/drakor.zip", NULL};
        execv("/bin/unzip", argv);
    }
}

void remove_folder(pid_t pid){
    int status;
    if(pid == 0) {
        char *args[] = {"/bin/bash","bash", "-c", "rm -r /home/aaliyah/shift2/drakor/*/", NULL};
        if(execv(args[0], args+1) == -1){
            perror("remove folder failed");
                exit(EXIT_FAILURE);
        }
    }
    else {((wait(&status))>0);}
}

void delete_substr(char *str, char *substr){
    char *comp;
    int png = strlen(substr);
    while((comp = strstr(str, substr))){
        *comp = '\0';
        strcat(str, comp+png);
    }
}

void add_data(char title[NAME_MAX], char name[NAME_MAX], char year[NAME_MAX]){
    FILE *fp;
    char addPath[PATH_MAX];
    char cName[NAME_MAX];
    char checkName[NAME_MAX];
    char checkTitle[NAME_MAX];
    char line[100000];


    strcpy(addPath, "/home/aaliyah/shift2/drakor/");
    strcat(addPath, name);
    strcat(addPath, "/data.txt");

    fp = fopen(addPath, "a+");

    strcpy(checkName, "kategori: ");
    strcat(checkName, name);
    fscanf(fp, "%[^\n]", cName);

    if (strcmp(cName, checkName) != 0){
        fprintf(fp, "kategori: %s\n\n", name);
    }

    strcpy(checkTitle, title);

    bool status = false;
    while(fgets(line, sizeof(line), fp)){

        if(strstr(line, checkTitle) != 0){
            status = true;
            break;
        }
    }

    if(!status){
        fprintf(fp, "nama: %s\nrilis: %s\n\n", title, year);
    }
    fclose(fp);
}

void copy_image(char *fileName, char title[NAME_MAX], char name[NAME_MAX], char year[NAME_MAX], char path[]){

    char from[PATH_MAX];
    strcpy(from, path);
    strcat(from, fileName);

    delete_substr(fileName, ".png");
    char *token;
    token = strtok(fileName, "_;");

    int flag = 0;
    while(token != NULL){
        if(flag == 0 | flag == 3) strcpy(title, token);

        if(flag == 1 | flag == 4) strcpy(year, token);

        if(flag == 2 | flag == 5) strcpy(name, token);

        flag++;
        token = strtok(NULL, "_;");
    }

    char dest[PATH_MAX];
    strcpy(dest, "/home/aaliyah/shift2/drakor/");
    strcat(dest, name);
    strcat(dest, "/");
    strcat(dest, title);
    strcat(dest, ".png");

    pid_t ch = fork();
    int status;

    if(ch < 0) exit(EXIT_FAILURE);

    if(ch == 0){
        char *argv[] = {"cp", from, dest, NULL};
        execv("/bin/cp", argv);
    }else{
        while((wait(&status)) > 0);
        add_data(title, name, year);
        return;
    }

}

void create_genre_dir(char name[NAME_MAX], char path[]){
    char folderName[PATH_MAX];
    strcpy(folderName, path);
    strcat(folderName, "/");
    strcat(folderName, name);

    pid_t ch = fork();
    int status;

    if (ch == 0){
        int len = strlen(name);
        if (len <= 8){
            char *argv[] = {"mkdir", "-p", folderName, NULL};
            execv("/bin/mkdir", argv);
        }
    }else{
        while((wait(&status)) > 0);
        return;
    }
}

void create_data(pid_t child_id){
    char cwd[PATH_MAX];
    getcwd(cwd, PATH_MAX);
    char current[PATH_MAX];

    DIR *dir;
    struct dirent *dp;
    char path[PATH_MAX];
    char myPath[PATH_MAX];
    int status;
    int status2;

    strcpy(current, cwd);

    dir = opendir(current);
    strcpy(path, "/home/aaliyah/shift2/drakor/");
    strcpy(myPath, current);
    strcat(myPath, "/");

    while((dp = readdir(dir)) != NULL){
        if(strcmp(dp->d_name, ".") != 0 && strcmp(dp->d_name, "..") != 0) {

            char fileName[PATH_MAX];
            strcpy(fileName, dp->d_name);

            delete_substr(fileName, ".png");

            char name[PATH_MAX];
            char title[PATH_MAX];
            char year[PATH_MAX];
            char *split;
            split = strtok(fileName, "_;");

            int flag = 0;
            while(split != NULL){

                if(flag == 0 | flag == 3){
                    strcpy(title, split);
                }

                if(flag == 2 | flag == 5){
                    strcpy(name, split);
                }

                if(flag == 1 | flag == 4){
                    strcpy(year, split);
                }

                if(strchr(dp->d_name, '/') == 0){
                    if(( strlen(title) > 0 & strlen(name) > 0 & strlen(year) > 0 )){
                        copy_image(dp->d_name, title, name, year, myPath);
                    }
                }
                split = strtok(NULL, "_;");
                flag++;
            }
        }
    }
}

void duplicate_image(){
    char cwd[PATH_MAX];
    getcwd(cwd, PATH_MAX);
    char current[PATH_MAX];

    DIR *dir;
    struct dirent *dp;
    char path[PATH_MAX];
    int status;

    strcpy(current, cwd);
    dir = opendir(current);

    while((dp = readdir(dir)) != NULL){
        if(strcmp(dp->d_name, ".") != 0 && strcmp(dp->d_name, "..") != 0){

            char fileName[NAME_MAX];
            strcpy(fileName, dp->d_name);
            delete_substr(fileName, ".png");

            strcpy(path, current);
            strcat(path, "/");

            char title[NAME_MAX];
            char name[NAME_MAX];
            char year[NAME_MAX];

            if(strchr(fileName, '_') != 0){
                char *token;
                token = strtok(fileName, "_;");

                int flag = 0;
                while(token != NULL){
                    if(flag == 0){
                        strcpy(title, token);
                    }

                    if(flag == 1){
                        strcpy(year, token);
                    }

                    if(flag == 2){
                        strcpy(name, token);
                    }

                    token = strtok(NULL, "_;");
                    flag++;
                }
            }else{continue;}

            strcat(path, dp->d_name);

            char dest[PATH_MAX];
            strcpy(dest, current);
            strcat(dest, "/");
            strcat(dest, title);
            strcat(dest, ";");
            strcat(dest, year);
            strcat(dest, ";");
            strcat(dest, name);
            strcat(dest, ".png");

            pid_t ch = fork();
            int status;

            if (ch < 0) exit(EXIT_FAILURE);

            if(ch == 0){
                char *argv[] = {"cp", path, dest, NULL};
                execv("/bin/cp", argv);
            }else{
                while((wait(&status)) > 0);
                ch = fork();
            }
        }
    }

    closedir(dir);
}

void create_genre(pid_t child_id){
    char cwd[PATH_MAX];
    getcwd(cwd, PATH_MAX);
    char current[PATH_MAX];

    DIR *dir;
    struct dirent *dp;
    char path[PATH_MAX];
    char myDir[PATH_MAX];
    int status;

    strcpy(current, cwd);

    dir = opendir(current);

    while((dp = readdir(dir)) != NULL){
        if(strcmp(dp->d_name, ".") != 0 && strcmp(dp->d_name, "..") != 0){

            if(child_id == 0){
                char fileName[PATH_MAX];
                strcpy(fileName, dp->d_name);

                delete_substr(fileName, ".png");

                char name[PATH_MAX];
                char *split;
                split = strtok(fileName, "_;");

                int flag = 0;
                while(split != NULL){
                    if(flag == 2){
                        strcpy(name, split);
                        int len = strlen(name);
                        strcpy(myDir, "/home/aaliyah/shift2/drakor/");
                        strcat(myDir, name);

                        char *argv[] = {"mkdir", "-p", myDir, NULL};
                        execv("/bin/mkdir", argv);
                    }
                    split = strtok(NULL, "_;");
                    flag++;
                }
            }
            while((wait(&status)) > 0);
            child_id = fork();
        }
    }

}

int main() {
    int status;
    pid_t child_id;

    child_id = fork();

    if (child_id < 0) {
        exit(EXIT_FAILURE);
    }

    char cwd[PATH_MAX];

    if ((chdir(getcwd(cwd, PATH_MAX))) < 0) {
        exit(EXIT_FAILURE);
    }

    if (child_id == 0){
        create_folder(fork());

    }else{
        while((wait(&status)) > 0);

        pid_t ch_del = fork();
        int statusDir;

        if(ch_del == 0){
            if (ch_del < 0) exit(EXIT_FAILURE);
            remove_folder(fork());
        }else{
            while((wait(&statusDir)) > 0);

            pid_t ch_gen = fork();
            int statusGen;

            if(ch_gen == 0){
                create_genre(fork());
            }else{
                while((wait(&statusGen)) > 0);

                pid_t ch_dup = fork();
                int statusdi;

                if(ch_dup < 0) exit(EXIT_FAILURE);

                if(ch_dup == 0){
                    duplicate_image();
                }else{
                    while((wait(&statusdi)) > 0);

                    pid_t ch_data = fork();
                    int statuscd;

                    if(ch_data < 0) exit(EXIT_FAILURE);

                    if(ch_data == 0){
                        create_data(fork());
                    }
                    else{
                        while((wait(&statuscd)) > 0);

			pid_t ch_del = fork();
			int statusdel;

			if(ch_del < 0) exit(EXIT_FAILURE);

                        if(ch_del == 0){
		            wait(0);
       			    char *args[] = {"/bin/bash","bash","-c","rm /home/aaliyah/shift2/drakor/*.png", NULL};
			    if(execv(args[0],args+1) == -1){
	               	        perror("remove failed");
        	     	        exit(EXIT_FAILURE);
		            }
                        }
                    }
                }
            }
	}
    }
}
