#include <sys/types.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <stdbool.h>
#include <errno.h>
#include <unistd.h>
#include <syslog.h>
#include <string.h>
#include <dirent.h>
#include <pwd.h>

int main() {
  pid_t pid, sid;        // Variabel untuk menyimpan PID

  pid = fork();     // Menyimpan PID dari Child Process
  int status, status2, status3, status4, status5, status6;

  /* Keluar saat fork gagal
  * (nilai variabel pid < 0) */
  if (pid < 0) {
    exit(EXIT_FAILURE);
  }

  /* Keluar saat fork berhasil
  * (nilai variabel pid adalah PID dari child process) */
  if (pid > 0) {
    exit(EXIT_SUCCESS);
  }

  //a. Membuat directory darat & air
  if(pid == 0){
     if (fork() == 0) execl("/bin/mkdir", "mkdir", "-p", "/home/nadya/modul2/darat", NULL);
	sleep(3);
     if (fork() == 0) execl("/bin/mkdir", "mkdir", "-p", "/home/nadya/modul2/air", NULL);
  }

  //b. Extract animal.zip
  if(pid == 0){
    if (fork() == 0){
      char *argv[] = {"unzip", "/home/nadya/animal.zip", "-d", "/home/nadya/modul2/", NULL};
      execv("/usr/bin/unzip", argv);
    }
  }
  while ((wait(&status)) > 0);

  //c. Pisah hewan darat & laut
  pid = fork();

  if(pid == 0){
    DIR *d;
    struct dirent *dir;
    char path[100] = {"/home/nadya/modul2/animal/"};
    d = opendir(path);

    if (d){
      while ((dir = readdir (d)) != NULL){
        if (strcmp(dir->d_name, ".") == 0 || strcmp(dir->d_name, "..") == 0) continue;
	char namafile[350];
	sprintf(namafile, "/home/nadya/modul2/animal/%s", dir->d_name);
        if(strstr(dir->d_name, "darat")){
	  if(fork() == 0){
  	    char *argv[] = {"mv", namafile, "/home/nadya/modul2/darat", NULL};
	    execv("/bin/mv", argv);
	    }
          }else if(strstr(dir->d_name, "air")){
	    if(fork() == 0){
	      char *argv[] = {"mv", namafile, "/home/nadya/modul2/air", NULL};
	      execv("/bin/mv", argv);
	    }
          }
	while ((wait(&status3)) > 0);
      }
      pid_t cid2;
      cid2 = fork();
      if(cid2 == 0){
        char *argv[] = {"rm", "-r", "/home/nadya/modul2/animal", NULL};
        execv("/bin/rm", argv);
      }
    closedir(d);
    }
  while ((wait(&status2)) > 0);
  }

  //d. Hapus bird
  pid = fork();
  if(pid == 0){
    DIR *d1;
    struct dirent *dir1;
    char path1[100] = {"/home/nadya/modul2/darat/"};
    d1 = opendir(path1);

    if (d1){
      while ((dir1 = readdir (d1)) != NULL){
	char namafile1[350];
	sprintf(namafile1, "/home/nadya/modul2/darat/%s", dir1->d_name);
	if(strstr(dir1->d_name, "bird")){
	  if(fork() == 0){
	    char *argv[] = {"rm", namafile1, NULL};
	    execv("/bin/rm", argv);
	  }
	}
      }
    closedir(d1);
    }
  while ((wait(&status4)) > 0);
  }

  //e. list.txt
  pid = fork();
  if(pid == 0){
    chdir("/home/nadya/modul2/air/");
      char *argv[] = {"usr/bin/touch", "list.txt", NULL};
      execv("/usr/bin/touch", argv);
  }
  while ((wait(&status5)) > 0);

  pid = fork();
  if(pid == 0){
    FILE * list;
    list = fopen ("/home/nadya/modul2/air/list.txt", "w+");
    int cek = 0;
    pid = fork();
    if(pid == 0){
      DIR *d12;
      struct dirent *dir12;
      char path12[100] = {"/home/nadya/modul2/air/"};
      d12 = opendir(path12);

      if (d12){
        while ((dir12 = readdir (d12)) != NULL){
  	  char namafile12[350];
	  sprintf(namafile12, "/home/nadya/modul2/darat/%s", dir12->d_name);
	  if(strstr(dir12->d_name, "jpg") || strstr(dir12->d_name, "png")){

	    struct stat info;
	    int r;
	    r = stat("/home/nadya/modul2/darat", &info);
	    if(r == -1){
	      fprintf(stderr, "File error\n");
	      exit(1);
	    }
	    struct passwd *pw = getpwuid(info.st_uid);
	    if(pw != 0) fprintf(list, "%s_" , pw->pw_name);
	    struct stat fs;
	    if( fs.st_mode & S_IRUSR )
	      fprintf(list, "r");
	    if( fs.st_mode & S_IWUSR )
	      fprintf(list, "w");
	    if( fs.st_mode & S_IXUSR )
	      fprintf(list, "x");


	    fprintf(list, "_%s\n", dir12->d_name);
	  }
        }
      closedir(d12);
      }
    while ((wait(&status6)) > 0);
    fclose(list);
    }
  }
}
