#include<stdio.h>
#include<stdlib.h>
#include<sys/stat.h>
#include<sys/types.h>
#include<wait.h>
#include<time.h>
#include<unistd.h>
#include<dirent.h>
#include<string.h>
#include<json-c/json.h>
#include<pwd.h>

#define ll long long

const ll tokenpg = 160;
ll primogems = 79000;
ll ngacha = 0, txt_cnt = 0;
int idc = 0, idw = 0;
char arrweap[500][100], arrchar[500][100], gacha_res[10][200] = {"", "", "", "", "", "", "", "", "", ""}, txt_res[10][200] = {"", "", "", "", "", "", "", "", "", ""};
char user[50];

int main() {
    strcpy(user, getenv("USER"));
    pid_t pid, sid;
    pid = fork();
    if(pid < 0) exit(EXIT_FAILURE);
    if(pid > 0) exit(EXIT_SUCCESS);
    umask(0);
    sid = setsid();
    if(sid < 0) exit(EXIT_FAILURE);
    char path[50] = "/home/";
    strcat(path, user);
    strcat(path, "/Documents/SISOP/SHIFT2/soal-shift-sisop-modul-2-c06-2022/soal1/");
    if ((chdir(path)) < 0) exit(EXIT_FAILURE);
    close(STDIN_FILENO);
    close(STDOUT_FILENO);
    close(STDERR_FILENO);
    while(1) {
        time_t t1 = time(NULL);
        struct tm tm = *localtime(&t1);
        char buffer_time[50];
        int buffer_time_len = sizeof(buffer_time);
        snprintf(buffer_time, buffer_time_len, "%02d-%02d %02d:%02d", tm.tm_mon + 1, tm.tm_mday, tm.tm_hour, tm.tm_min);
        char buffer_exc[50] = "03-30 04:44";
        if(!strcmp(buffer_time, buffer_exc)){//
            pid_t char_ch;
            char_ch = fork();
            if(char_ch < 0){
                exit(EXIT_FAILURE);
            }
            if(!char_ch){
                pid_t weap_ch;
                weap_ch = fork();
                if(weap_ch < 0){
                    exit(EXIT_FAILURE);
                }
                if(!weap_ch){
                    pid_t hopebe1_ch;
                    hopebe1_ch = fork();
                    if(!hopebe1_ch){
                        execl("/usr/bin/wget", "wget", "-O", "./weap.zip", "https://drive.google.com/u/0/uc?id=1XSkAqqjkNmzZ0AdIZQt_eWGOZ0eJyNlT&export=download", "-qq", NULL);
                    }else{
                        wait(0);
                        execl("/usr/bin/wget", "wget", "-O", "./char.zip", "https://drive.google.com/u/0/uc?id=1xYYmsslb-9s8-4BDvosym7R4EmPi6BHp&export=download", "-qq",  NULL);
                    }
                }else{
                    wait(0);
                    pid_t mkdir_gachaf_ch;
                    mkdir_gachaf_ch = fork();
                    if(!mkdir_gachaf_ch) execl("/usr/bin/mkdir", "mkdir", "./gacha_gacha", NULL);
                    else{
                        wait(0);
                        pid_t inilastya;
                        inilastya = fork();
                        if(!inilastya) execl("/usr/bin/unzip", "unzip", "-qq", "char.zip", NULL);
                        else{
                            wait(0);
                            execl("/usr/bin/unzip", "unzip", "-qq", "weap.zip", NULL);
                        }
                    }
                }
            }else{
                wait(0);
                pid_t rejson;
                rejson = fork();
                if(!rejson){
                    DIR *dp;
                    struct dirent *ep;
                    char path[2][100] = {"./characters", "./weapons"};
                    int okeid = 2;
                    while(okeid){
                        dp = opendir(path[okeid - 1]);
                        if (dp != NULL)
                        {
                            while ((ep = readdir (dp))) {
                                if(okeid == 2){
                                    strcpy(arrweap[idw], ep->d_name);
                                    idw++;
                                }else{
                                    strcpy(arrchar[idc], ep->d_name);
                                    idc++;
                                }
                            }
                            (void) closedir (dp);
                        } else perror ("Couldn't open the directory");
                        okeid--;
                    }
                    FILE *fp;
                    char buffer[3000];
                    struct json_object *parsed_json;
                    struct json_object *name;
                    struct json_object *rarity;
                    size_t cnt;
                    while(primogems >= 160){
                        primogems -= 160;
                        ngacha++;
                        ll gopay_char = 0, gopay_weap = 0;
                        char gj1[] = "..";
                        char gj2[] = ".";
                        while(!gopay_char || !strcmp(gj1, arrchar[gopay_char]) || !strcmp(gj2, arrchar[gopay_char])) gopay_char = rand() % 47;
                        while(!gopay_weap || !strcmp(gj1, arrweap[gopay_weap]) || !strcmp(gj2, arrweap[gopay_weap])) gopay_weap = rand() % 47;
                        char itu_char[50] = "characters/";
                        char itu_weap[50] = "weapons/";
                        strcat(itu_char, arrchar[gopay_char]);
                        strcat(itu_weap, arrweap[gopay_weap]);
                        if(ngacha & 1)fp = fopen(itu_char, "r");
                        else fp = fopen(itu_weap, "r");
                        fread(buffer, 3000, 1, fp);
                        fclose(fp);
                        parsed_json = json_tokener_parse(buffer);
                        json_object_object_get_ex(parsed_json, "name", &name);
                        json_object_object_get_ex(parsed_json, "rarity", &rarity);
                        char buffer[50];
                        int buffer_len = sizeof(buffer);
                        snprintf(buffer, buffer_len, "%lld", ngacha);
                        ll ngachamod = ngacha % 10;
                        strcpy(gacha_res[ngachamod], "");
                        strcat(gacha_res[ngachamod], buffer);
                        if(ngacha & 1) strcat(gacha_res[ngachamod], "_characters_");
                        else strcat(gacha_res[ngachamod], "_weapons_");
                        strcat(gacha_res[ngachamod], json_object_get_string(rarity));
                        strcat(gacha_res[ngachamod], "_");
                        strcat(gacha_res[ngachamod], json_object_get_string(name));
                        strcat(gacha_res[ngachamod], "_");
                        char buffer1[50];
                        int buffer_len1 = sizeof(buffer1);
                        snprintf(buffer1, buffer_len1, "%lld", primogems);
                        strcat(gacha_res[ngachamod], buffer1);
                        if(!(ngacha % 10)){
                            sleep(1);
                            txt_cnt++;
                            time_t t = time(NULL);
                            struct tm tm = *localtime(&t);
                            char buffer2[50];
                            int buffer_len2 = sizeof(buffer2);
                            snprintf(buffer2, buffer_len2, "./gacha_gacha/%02d:%02d:%02d_gacha_%lld.txt", tm.tm_hour, tm.tm_min, tm.tm_sec, ngacha);
                            strcpy(txt_res[txt_cnt], "");
                            strcat(txt_res[txt_cnt], buffer2);
                            FILE *fp1;
                            fp1 = fopen (buffer2, "w+");
                            for(ll i = 1; i < 10; i++){
                                fputs(gacha_res[i], fp1);
                                fputs("\n", fp1);
                            }
                            fputs(gacha_res[0], fp1);
                            fputs("\n", fp1);
                            fclose(fp1);
                        }
                        if(!(ngacha % 90)){
                            txt_cnt = 0;
                            char buffer3[50];
                            int buffer_len3 = sizeof(buffer3);
                            snprintf(buffer3, buffer_len3, "./gacha_gacha/total_gacha_%lld", ngacha - 90);
                            pid_t mkdir_id;
                            mkdir_id = fork();
                            if (mkdir_id < 0) {
                                exit(EXIT_FAILURE);
                            }
                            if (mkdir_id == 0) {
                                execl("/usr/bin/mkdir", "mkdir", "-p", buffer3, NULL);
                            }else {
                                wait(0);
                                for(ll i = 1; i <= 9; i++){
                                    pid_t mv_file;
                                    mv_file = fork();
                                    if(mv_file < 0) exit(EXIT_FAILURE);
                                    if(!mv_file){
                                        char buffer4[50];
                                        int buffer_len4 = sizeof(buffer4);
                                        snprintf(buffer4, buffer_len4, "%s", txt_res[i]);
                                        execl("/usr/bin/mv", "mv", buffer4, buffer3, NULL);
                                    }
                                }
                            }
                        }
                    }
                    char buffer_end[50] = "03-30 07:44";
                    while(1){
                        time_t t5 = time(NULL);
                        struct tm tm = *localtime(&t5);
                        char buffer_time1[50];
                        int buffer_time_len1 = sizeof(buffer_time1);
                        snprintf(buffer_time1, buffer_time_len1, "%02d-%02d %02d:%02d", tm.tm_mon + 1, tm.tm_mday, tm.tm_hour, tm.tm_min);
                        if(!strcmp(buffer_end, buffer_time1)) 
                        execl("/usr/bin/zip", "zip", "-r", "-e", "-P", "satuduatiga", "not_safe_for_wibu", "gacha_gacha", "-q", NULL);
                        sleep(10);
                    }
                }else{
                    wait(0);
                    pid_t rm_rem;
                    rm_rem = fork();
                    if(!rm_rem) execl("/usr/bin/rm", "rm", "./char.zip", "./weap.zip", NULL);
                    else{
                        wait(0);
                        execl("/usr/bin/rm", "rm", "-r", "gacha_gacha", "characters", "weapons", NULL);
                    }
                }
            }
            sleep(30);
        }//
    }
}