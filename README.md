<h1>[LAPORAN RESMI MODUL 2 KELOMPOK C06]</h1>

<h2>Daftar Isi</h2>

- [Soal 1](#soal-1) <br>
	- [Soal 1a](#soal-1a)
	- [Soal 1b](#soal-1b)
	- [Soal 1c](#soal-1c)
	- [Soal 1d](#soal-1d)
	- [Soal 1e](#soal-1e)
- [Soal 2](#soal-2) <br>
	- [Soal 2a](#soal-2a)
	- [Soal 2b](#soal-2b)
	- [Soal 2c](#soal-2c-dan-2d)
	- [Soal 2d](#soal-2e)
- [Soal 3](#soal-3) <br>
	- [Soal 3a](#soal-3a)
	- [Soal 3b](#soal-3b)
	- [Soal 3c](#soal-3c)
	- [Soal 3d](#soal-3d)
	- [Soal 3e](#soal-3e)
- [Kendala](#kendala)
	- [Kendala Soal 1](#kendala-soal-1-)
	- [Kendala Soal 2](#kendala-soal-2-)
	- [Kendala Soal 3](#kendala-soal-3-)

<h3>Soal 1</h3>
<strong>Deskripsi </strong>

Simulasi gacha game benshin impek menggunakan database berekstensi .json yang terlampir pada file .zip, terdiri dari weapons dan characters

<h4>Soal 1a</h4>

Meng<i>download</i> dan mengekstrak .zip characters & weapons serta membuat direktori gacha_gacha untuk menampung hasil gacha

Masing-masing perintah karena menggunakan bash command, dan padahal tidak boleh menggunakan system(), maka kita gunakan exec. Karena exec akan menghentikan process saat ini, kita akan menaruh tiap-tiap perintah tadi pada percabangan fork() yang telah dibuat yang banyaknya sesuai banyaknya proccess yang akan kita jalankan.

```
	pid_t char_ch;
	char_ch = fork();
	if(char_ch < 0){
		exit(EXIT_FAILURE);
	}
	if(!char_ch){
		pid_t weap_ch;
		weap_ch = fork();
		if(weap_ch < 0){
			exit(EXIT_FAILURE);
		}
		if(!weap_ch){
			pid_t hopebe1_ch;
			hopebe1_ch = fork();
			if(!hopebe1_ch){
				execl("/usr/bin/wget", "wget", "-O", "./weap.zip", "https://drive.google.com/u/0/uc?id=1XSkAqqjkNmzZ0AdIZQt_eWGOZ0eJyNlT&export=download", "-qq", NULL);
			}else{
				wait(0);
				execl("/usr/bin/wget", "wget", "-O", "./char.zip", "https://drive.google.com/u/0/uc?id=1xYYmsslb-9s8-4BDvosym7R4EmPi6BHp&export=download", "-qq",  NULL);
			}
		}else{
			wait(0);
			pid_t mkdir_gachaf_ch;
			mkdir_gachaf_ch = fork();
			if(!mkdir_gachaf_ch) execl("/usr/bin/mkdir", "mkdir", "./gacha_gacha", NULL);
			else{
				wait(0);
				pid_t inilastya;
				inilastya = fork();
				if(!inilastya) execl("/usr/bin/unzip", "unzip", "-qq", "char.zip", NULL);
				else{
					wait(0);
					execl("/usr/bin/unzip", "unzip", "-qq", "weap.zip", NULL);
				}
			}
		}
```
Hasilnya akan terlihat seperti berikut :
<img src="foto/soal1/2-1a1.png" alt="2-1a1">

<h4>Soal 1b</h4>

Membuat file ekstensi .txt yang merupakan kumpulan dari tiap 10 hasil gacha, kemudian tiap 9 file .txt hasil gacha yang terbentuk, masukkan ke dalam folder baru. Untuk implementasi per-requestnya akan dijelaskan pada sub-soal c dan d. Di sini hanya akan memperlihatkan hasil dari permintaan soal

<img src="foto/soal1/2-1b1.png" alt="2-1b1">
<img src="foto/soal1/2-1b2.png" alt="2-1b2">
<img src="foto/soal1/2-1b3.png" alt="2-1b3">
<img src="foto/soal1/2-1b4.png" alt="2-1b4">

<h4>Soal 1c</h4>

Format penamaan file ekstensi .txt yang merupakan kumpulan dari hasil 10 gacha tiap filenya dan format penamaan folder tiap hasil gacha nya merupakan kelipatan dari 90 kali gacha.
```
if(!(ngacha % 10)){
	sleep(1);
	txt_cnt++;
	time_t t = time(NULL);
	struct tm tm = *localtime(&t);
	char buffer2[50];
	int buffer_len2 = sizeof(buffer2);
	snprintf(buffer2, buffer_len2, "./gacha_gacha/%02d:%02d:%02d_gacha_%lld.txt", tm.tm_hour, tm.tm_min, tm.tm_sec, ngacha);
	strcpy(txt_res[txt_cnt], "");
	strcat(txt_res[txt_cnt], buffer2);
	FILE *fp1;
	fp1 = fopen (buffer2, "w+");
	for(ll i = 1; i < 10; i++){
		fputs(gacha_res[i], fp1);
		fputs("\n", fp1);
	}
	fputs(gacha_res[0], fp1);
	fputs("\n", fp1);
	fclose(fp1);
}
if(!(ngacha % 90)){
	txt_cnt = 0;
	char buffer3[50];
	int buffer_len3 = sizeof(buffer3);
	snprintf(buffer3, buffer_len3, "./gacha_gacha/total_gacha_%lld", ngacha - 90);
	pid_t mkdir_id;
	mkdir_id = fork();
	if (mkdir_id < 0) {
		exit(EXIT_FAILURE);
	}
	if (mkdir_id == 0) {
		execl("/usr/bin/mkdir", "mkdir", "-p", buffer3, NULL);
	}else {
		wait(0);
		for(ll i = 1; i <= 9; i++){
			pid_t mv_file;
			mv_file = fork();
			if(mv_file < 0) exit(EXIT_FAILURE);
			if(!mv_file){
				char buffer4[50];
				int buffer_len4 = sizeof(buffer4);
				snprintf(buffer4, buffer_len4, "%s", txt_res[i]);
				execl("/usr/bin/mv", "mv", buffer4, buffer3, NULL);
			}
		}
	}
}

```
Hasilnya akan seperti berikut :
<img src="foto/soal1/2-1c1.png" alt="2-1c1">
<img src="foto/soal1/2-1c2.png" alt="2-1c2">

<h4>Soal 1d</h4>

Format hasil gacha pada tiap kali melakukan gacha, serta mensimulasikan gacha dari state awal 79000 hingga habis tidak bisa melakukan gacha lagi.
```
while(primogems >= 160){
	primogems -= 160;
	ngacha++;
	ll gopay_char = 0, gopay_weap = 0;
	char gj1[] = "..";
	char gj2[] = ".";
	while(!gopay_char || !strcmp(gj1, arrchar[gopay_char]) || !strcmp(gj2, arrchar[gopay_char])) gopay_char = rand() % 47;
	while(!gopay_weap || !strcmp(gj1, arrweap[gopay_weap]) || !strcmp(gj2, arrweap[gopay_weap])) gopay_weap = rand() % 47;
	char itu_char[50] = "characters/";
	char itu_weap[50] = "weapons/";
	strcat(itu_char, arrchar[gopay_char]);
	strcat(itu_weap, arrweap[gopay_weap]);
	if(ngacha & 1)fp = fopen(itu_char, "r");
	else fp = fopen(itu_weap, "r");
	fread(buffer, 3000, 1, fp);
	fclose(fp);
	parsed_json = json_tokener_parse(buffer);
	json_object_object_get_ex(parsed_json, "name", &name);
	json_object_object_get_ex(parsed_json, "rarity", &rarity);
	char buffer[50];
	int buffer_len = sizeof(buffer);
	snprintf(buffer, buffer_len, "%lld", ngacha);
	ll ngachamod = ngacha % 10;
	strcpy(gacha_res[ngachamod], "");
	strcat(gacha_res[ngachamod], buffer);
	if(ngacha & 1) strcat(gacha_res[ngachamod], "_characters_");
	else strcat(gacha_res[ngachamod], "_weapons_");
	strcat(gacha_res[ngachamod], json_object_get_string(rarity));
	strcat(gacha_res[ngachamod], "_");
	strcat(gacha_res[ngachamod], json_object_get_string(name));
	strcat(gacha_res[ngachamod], "_");
	char buffer1[50];
	int buffer_len1 = sizeof(buffer1);
	snprintf(buffer1, buffer_len1, "%lld", primogems);
	strcat(gacha_res[ngachamod], buffer1);
	if(!(ngacha % 10)){
		sleep(1);
		txt_cnt++;
		time_t t = time(NULL);
		struct tm tm = *localtime(&t);
		char buffer2[50];
		int buffer_len2 = sizeof(buffer2);
		snprintf(buffer2, buffer_len2, "./gacha_gacha/%02d:%02d:%02d_gacha_%lld.txt", tm.tm_hour, tm.tm_min, tm.tm_sec, ngacha);
		strcpy(txt_res[txt_cnt], "");
		strcat(txt_res[txt_cnt], buffer2);
		FILE *fp1;
		fp1 = fopen (buffer2, "w+");
		for(ll i = 1; i < 10; i++){
			fputs(gacha_res[i], fp1);
			fputs("\n", fp1);
		}
		fputs(gacha_res[0], fp1);
		fputs("\n", fp1);
		fclose(fp1);
	}
	if(!(ngacha % 90)){
		txt_cnt = 0;
		char buffer3[50];
		int buffer_len3 = sizeof(buffer3);
		snprintf(buffer3, buffer_len3, "./gacha_gacha/total_gacha_%lld", ngacha - 90);
		pid_t mkdir_id;
		mkdir_id = fork();
		if (mkdir_id < 0) {
			exit(EXIT_FAILURE);
		}
		if (mkdir_id == 0) {
			execl("/usr/bin/mkdir", "mkdir", "-p", buffer3, NULL);
		}else {
			wait(0);
			for(ll i = 1; i <= 9; i++){
				pid_t mv_file;
				mv_file = fork();
				if(mv_file < 0) exit(EXIT_FAILURE);
				if(!mv_file){
					char buffer4[50];
					int buffer_len4 = sizeof(buffer4);
					snprintf(buffer4, buffer_len4, "%s", txt_res[i]);
					execl("/usr/bin/mv", "mv", buffer4, buffer3, NULL);
				}
			}
		}
	}
}
```
Hasilnya akan seperti berikut :
<img src="foto/soal1/2-1d.png" alt="2-1d">

<h4>Soal 1e</h4>

Melakukan gacha sesuatu waktu pertama kali problem setter memainkan game benshin impek, dan mengzip semua hasil gacha serta mengenkripsinya pada 3 jam setelahnya.

<strong>Kode untuk memulai program daemonnya pada tanggal sesuai soal</strong>
```
strcpy(user, getenv("USER"));
pid_t pid, sid;
pid = fork();
if(pid < 0) exit(EXIT_FAILURE);
if(pid > 0) exit(EXIT_SUCCESS);
umask(0);
sid = setsid();
if(sid < 0) exit(EXIT_FAILURE);
char path[50] = "/home/";
strcat(path, user);
strcat(path, "/Documents/SISOP/SHIFT2/soal-shift-sisop-modul-2-c06-2022/soal1/");
if ((chdir(path)) < 0) exit(EXIT_FAILURE);
close(STDIN_FILENO);
close(STDOUT_FILENO);
close(STDERR_FILENO);
while(1) {
	time_t t1 = time(NULL);
	struct tm tm = *localtime(&t1);
	char buffer_time[50];
	int buffer_time_len = sizeof(buffer_time);
	snprintf(buffer_time, buffer_time_len, "%02d-%02d %02d:%02d", tm.tm_mon + 1, tm.tm_mday, tm.tm_hour, tm.tm_min);
	char buffer_exc[50] = "03-30 04:44";
	if(!strcmp(buffer_time, buffer_exc)){
		// isi kode daemon
	}
}
```
<img src="foto/soal1/2-1e1.png" alt="2-1e1">

<strong>Program untuk meng-zip kembali semua hasil gacha yang ada lalu mengenkripsinya</strong>
```
char buffer_end[50] = "03-30 07:44";
while(1){
	time_t t5 = time(NULL);
	struct tm tm = *localtime(&t5);
	char buffer_time1[50];
	int buffer_time_len1 = sizeof(buffer_time1);
	snprintf(buffer_time1, buffer_time_len1, "%02d-%02d %02d:%02d", tm.tm_mon + 1, tm.tm_mday, tm.tm_hour, tm.tm_min);
	if(!strcmp(buffer_end, buffer_time1)) 
	execl("/usr/bin/zip", "zip", "-r", "-e", "-P", "satuduatiga", "not_safe_for_wibu", "gacha_gacha", "-q", NULL);
	sleep(10);
}
```
<img src="foto/soal1/2-1e2.png" alt="2-1e2">

<br>

<h3>Soal 2</h3>
<strong> Japrun bekerja di sebuah perusahaan dibidang review industri perfilman. Dia mendapatkan sebuah proyek untuk mencari drama korea yang tayang dan sedang ramai di Layanan Streaming Film untuk diberi review. Japrun sudah mendapat foto-foto poster serial dalam bentuk zip untuk diberikan review, tetapi didalam zip tersebut banyak sekali poster drama korea dan dia harus memisahkan
poster-poster drama korea tersebut tergantung dengan kategorinya. </strong>

<h4>Soal 2a</h4>
Membuat folder "/home/[user]/shift2/drakor" dan mengextract zip yang diberikan ke dalam folder "/home/[user]/shift2/drakor". Berikut fungsi create_folder yang digunakan untuk membuat folder dan mengextract zip yang diberikan ke dalam folder.

```
void create_folder(pid_t pid){
    int status;

    if (pid == 0) {
        char *argv[] = {"mkdir", "-p", "/home/aaliyah/shift2/drakor", NULL};
        execv("/bin/mkdir", argv);
    }else{
        while((wait(&status)) > 0);
        char *argv[] = {"unzip", "/home/aaliyah/sisop/modul2-soal2/drakor.zip", NULL};
        execv("/bin/unzip", argv);
    }
}
```

Maka pada directory akan nampak : 

<img src="foto/soal2/2a(1).png" alt="2a(1)">

Memilah file yang penting. Di dalam zip tersebut file yang penting berbuat .png dan lainnya yang tidak penting adalah folder. Berikut fungsi remove_folder yang digunakan untuk menghapus folder.

```
void remove_folder(pid_t pid){
    int status;
    if(pid == 0) {
        char *args[] = {"/bin/bash","bash", "-c", "rm -r /home/aaliyah/shift2/drakor/*/", NULL};
        if(execv(args[0], args+1) == -1){
            perror("remove folder failed");
                exit(EXIT_FAILURE);
        }
    }
    else {((wait(&status))>0);}
}
```

Maka pada directory akan nampak : 

<img src="foto/soal2/2a(2).png" alt="2a(2)">

<h4>Soal 2b</h4>
Membuat folder zip sesuai category yang ada. Berikut fungsi create_genre yang digunakan untuk membuat folder-folder genre.

```
void create_genre(pid_t child_id){
    char cwd[PATH_MAX];
    getcwd(cwd, PATH_MAX);
    char current[PATH_MAX];

    DIR *dir;
    struct dirent *dp;
    char path[PATH_MAX];
    char myDir[PATH_MAX];
    int status;

    strcpy(current, cwd);

    dir = opendir(current);

    while((dp = readdir(dir)) != NULL){
        if(strcmp(dp->d_name, ".") != 0 && strcmp(dp->d_name, "..") != 0){

            if(child_id == 0){
                char fileName[PATH_MAX];
                strcpy(fileName, dp->d_name);

                delete_substr(fileName, ".png");

                char name[PATH_MAX];
                char *split;
                split = strtok(fileName, "_;");

                int flag = 0;
                while(split != NULL){
                    if(flag == 2){
                        strcpy(name, split);
                        int len = strlen(name);
                        strcpy(myDir, "/home/aaliyah/shift2/drakor/");
                        strcat(myDir, name);

                        char *argv[] = {"mkdir", "-p", myDir, NULL};
                        execv("/bin/mkdir", argv);
                    }
                    split = strtok(NULL, "_;");
                    flag++;
                }
            }
            while((wait(&status)) > 0);
            child_id = fork();
        }
    }
}
```

Pada fungsi create_genre, juga memanggil fungsi delete_substr

```
void delete_substr(char *str, char *substr){
    char *comp;
    int png = strlen(substr);
    while((comp = strstr(str, substr))){
        *comp = '\0';
        strcat(str, comp+png);
    }
}
```

Maka pada directory akan nampak : 
<img src="foto/soal2/2b.png" alt="2b">

<h4>Soal 2c dan 2d</h4>
Setelah folder kategori berhasil dibuat, program akan memindahkan poster ke folder dengan kategori yang sesuai. Berikut fungsi duplicate_image yang digunakan untuk memindahkan poster. Di dalam fungsi duplicate_image, juga memanggil fungsi delete_substr.

```
void duplicate_image(){
    char cwd[PATH_MAX];
    getcwd(cwd, PATH_MAX);
    char current[PATH_MAX];

    DIR *dir;
    struct dirent *dp;
    char path[PATH_MAX];
    int status;

    strcpy(current, cwd);
    dir = opendir(current);

    while((dp = readdir(dir)) != NULL){
        if(strcmp(dp->d_name, ".") != 0 && strcmp(dp->d_name, "..") != 0){

            char fileName[NAME_MAX];
            strcpy(fileName, dp->d_name);
            delete_substr(fileName, ".png");

            strcpy(path, current);
            strcat(path, "/");

            char title[NAME_MAX];
            char name[NAME_MAX];
            char year[NAME_MAX];

            if(strchr(fileName, '_') != 0){
                char *token;
                token = strtok(fileName, "_;");

                int flag = 0;
                while(token != NULL){
                    if(flag == 0){
                        strcpy(title, token);
                    }

                    if(flag == 1){
                        strcpy(year, token);
                    }

                    if(flag == 2){
                        strcpy(name, token);
                    }

                    token = strtok(NULL, "_;");
                    flag++;
                }
            }else{continue;}

            strcat(path, dp->d_name);

            char dest[PATH_MAX];
            strcpy(dest, current);
            strcat(dest, "/");
            strcat(dest, title);
            strcat(dest, ";");
            strcat(dest, year);
            strcat(dest, ";");
            strcat(dest, name);
            strcat(dest, ".png");

            pid_t ch = fork();
            int status;

            if (ch < 0) exit(EXIT_FAILURE);

            if(ch == 0){
                char *argv[] = {"cp", path, dest, NULL};
                execv("/bin/cp", argv);
            }else{
                while((wait(&status)) > 0);
                ch = fork();
            }
        }
    }
	closedir(dir);
}
```

Maka pada directory akan nampak : 
Folder Action:
<img src="foto/soal2/2d(action).png" alt="2d">
Folder Comedy:
<img src="foto/soal2/2d(comedy).png" alt="2d">
Folder Fantasy:
<img src="foto/soal2/2d(fantasy).png" alt="2d">
Folder Horror:
<img src="foto/soal2/2d(horror).png" alt="2d">
Folder Romance:
<img src="foto/soal2/2d(romance).png" alt="2d">
Folder School:
<img src="foto/soal2/2d(school).png" alt="2d">

<h4>Soal 2e</h4>
Setelah file dengan nama data.txt pada setiap folder kategori menggunakan fungsi create_data. 

```
void create_data(pid_t child_id){
    char cwd[PATH_MAX];
    getcwd(cwd, PATH_MAX);
    char current[PATH_MAX];

    DIR *dir;
    struct dirent *dp;
    char path[PATH_MAX];
    char myPath[PATH_MAX];
    int status;
    int status2;

    strcpy(current, cwd);

    dir = opendir(current);
    strcpy(path, "/home/aaliyah/shift2/drakor/");
    strcpy(myPath, current);
    strcat(myPath, "/");

    while((dp = readdir(dir)) != NULL){
        if(strcmp(dp->d_name, ".") != 0 && strcmp(dp->d_name, "..") != 0) {

            char fileName[PATH_MAX];
            strcpy(fileName, dp->d_name);

            delete_substr(fileName, ".png");

            char name[PATH_MAX];
            char title[PATH_MAX];
            char year[PATH_MAX];
            char *split;
            split = strtok(fileName, "_;");

            int flag = 0;
            while(split != NULL){

                if(flag == 0 | flag == 3){
                    strcpy(title, split);
                }

                if(flag == 2 | flag == 5){
                    strcpy(name, split);
                }

                if(flag == 1 | flag == 4){
                    strcpy(year, split);
                }

                if(strchr(dp->d_name, '/') == 0){
                    if(( strlen(title) > 0 & strlen(name) > 0 & strlen(year) > 0 )){
                        copy_image(dp->d_name, title, name, year, myPath);
                    }
                }
                split = strtok(NULL, "_;");
                flag++;
            }
        }
    }
}
```

Di dalam fungsi ini juga memanggil, fungsi copy_image dan fungsi add_data. Fungsi copy_image :

```
void copy_image(char *fileName, char title[NAME_MAX], char name[NAME_MAX], char year[NAME_MAX], char path[]){

    char from[PATH_MAX];
    strcpy(from, path);
    strcat(from, fileName);

    delete_substr(fileName, ".png");
    char *token;
    token = strtok(fileName, "_;");

    int flag = 0;
    while(token != NULL){
        if(flag == 0 | flag == 3) strcpy(title, token);

        if(flag == 1 | flag == 4) strcpy(year, token);

        if(flag == 2 | flag == 5) strcpy(name, token);

        flag++;
        token = strtok(NULL, "_;");
    }

    char dest[PATH_MAX];
    strcpy(dest, "/home/aaliyah/shift2/drakor/");
    strcat(dest, name);
    strcat(dest, "/");
    strcat(dest, title);
    strcat(dest, ".png");

    pid_t ch = fork();
    int status;

    if(ch < 0) exit(EXIT_FAILURE);

    if(ch == 0){
        char *argv[] = {"cp", from, dest, NULL};
        execv("/bin/cp", argv);
    }else{
        while((wait(&status)) > 0);
        add_data(title, name, year);
        return;
    }

}
```

Fungsi add_data :

```
void add_data(char title[NAME_MAX], char name[NAME_MAX], char year[NAME_MAX]){
    FILE *fp;
    char addPath[PATH_MAX];
    char cName[NAME_MAX];
    char checkName[NAME_MAX];
    char checkTitle[NAME_MAX];
    char line[100000];


    strcpy(addPath, "/home/aaliyah/shift2/drakor/");
    strcat(addPath, name);
    strcat(addPath, "/data.txt");

    fp = fopen(addPath, "a+");

    strcpy(checkName, "kategori: ");
    strcat(checkName, name);
    fscanf(fp, "%[^\n]", cName);

    if (strcmp(cName, checkName) != 0){
        fprintf(fp, "kategori: %s\n\n", name);
    }

    strcpy(checkTitle, title);

    bool status = false;
    while(fgets(line, sizeof(line), fp)){

        if(strstr(line, checkTitle) != 0){
            status = true;
            break;
        }
    }

    if(!status){
        fprintf(fp, "nama: %s\nrilis: %s\n\n", title, year);
    }
    fclose(fp);
}
```

Maka pada directory akan nampak seperti:
<img src="foto/soal2/2e.png" alt="2e">

<h3>Soal 3</h3>
<strong> Conan adalah seorang detektif terkenal. Suatu hari, Conan menerima beberapa laporan tentang hewan di kebun binatang yang tiba-tiba hilang. Karena jenis-jenis hewan yang hilang banyak, maka perlu melakukan klasifikasi hewan apa saja yang hilang. </strong>

<h4>Soal 3a</h4>
<strong> Untuk mempercepat klasifikasi, Conan diminta membuat program untuk membuat 2 directory di “/home/[USER]/modul2/” dengan nama “darat” lalu 3 detik kemudian membuat directory ke 2 dengan nama “air”. </strong>
<br><br>
Untuk soal 3a, kita akan membuat 2 directory dengan selang waktu 3 detik, maka yang kita gunakan adalah sleep(3), seperti :


```
  if(pid == 0){
     if (fork() == 0) execl("/bin/mkdir", "mkdir", "-p", "/home/nadya/modul2/darat", NULL);
	sleep(3);
     if (fork() == 0) execl("/bin/mkdir", "mkdir", "-p", "/home/nadya/modul2/air", NULL);
  }
```

Maka pada directory modul2 akan nampak : 
<img src="foto/soal3/3a.png" alt="3a">

<h4>Soal 3b</h4>
<strong> Kemudian program diminta dapat melakukan extract “animal.zip” di “/home/[USER]/modul2/”. </strong>
<br><br>
Selanjutnya, kita akan meng-extract animal.zip di dalam directory modul2, program yang dijalankan akan menggunakan execl :


```
if(pid == 0){
    if (fork() == 0){
      char *argv[] = {"unzip", "/home/nadya/animal.zip", "-d", "/home/nadya/modul2/", NULL};
      execv("/usr/bin/unzip", argv);
    }
  }
while ((wait(&status)) > 0);
```

<h4>Soal 3c</h4>
<strong> Tidak hanya itu, hasil extract dipisah menjadi hewan darat dan hewan air sesuai dengan nama filenya. Untuk hewan darat dimasukkan ke folder “/home/[USER]/modul2/darat” dan untuk hewan air dimasukkan ke folder “/home/[USER]/modul2/air”. Rentang pembuatan antara folder darat dengan folder air adalah 3 detik dimana folder darat dibuat terlebih dahulu. Untuk hewan yang tidak ada keterangan air atau darat harus dihapus. </strong>
<br><br>
Langkah berikutnya, memisah hasil extract. Hewan darat akan dimasukkan ke folder “/home/nadya/modul2/darat” dan untuk hewan air dimasukkan ke folder “/home/nadya/modul2/air”. Disini kita akan memakai execv. Program yang ditulis adalah :

```
while ((dir = readdir (d)) != NULL){
        if (strcmp(dir->d_name, ".") == 0 || strcmp(dir->d_name, "..") == 0) continue;
	char namafile[350];
	sprintf(namafile, "/home/nadya/modul2/animal/%s", dir->d_name);
        if(strstr(dir->d_name, "darat")){
	  if(fork() == 0){
  	    char *argv[] = {"mv", namafile, "/home/nadya/modul2/darat", NULL};
	    execv("/bin/mv", argv);
	    }
          }else if(strstr(dir->d_name, "air")){
	    if(fork() == 0){
	      char *argv[] = {"mv", namafile, "/home/nadya/modul2/air", NULL};
	      execv("/bin/mv", argv);
	    }
          }
	while ((wait(&status3)) > 0);
}
```

Lalu, untuk menghapus sisanya adalah : 

```
      if(cid2 == 0){
        char *argv[] = {"rm", "-r", "/home/nadya/modul2/animal", NULL};
        execv("/bin/rm", argv);
      }
```

<h4>Soal 3d</h4>
<strong> Setelah berhasil memisahkan hewan berdasarkan hewan darat atau hewan air. Dikarenakan jumlah burung yang ada di kebun binatang terlalu banyak, maka pihak kebun binatang harus merelakannya sehingga conan harus menghapus semua burung yang ada di directory “/home/[USER]/modul2/darat”. Hewan burung ditandai dengan adanya “bird” pada nama file. </strong>
<br><br>
Setelah itu, akan menghapus file yang mengandung kata "bird" dengan execv, seperti yang terlihat pada potongan program : 

```
while ((dir1 = readdir (d1)) != NULL){
	char namafile1[350];
	sprintf(namafile1, "/home/nadya/modul2/darat/%s", dir1->d_name);
	if(strstr(dir1->d_name, "bird")){
	  if(fork() == 0){
	    char *argv[] = {"rm", namafile1, NULL};
	    execv("/bin/rm", argv);
	  }
	}
}
```

Dalam folder akan terlihat seperti berikut :
<img src="foto/soal3/3d.png" alt="3d">

<h4>Soal 3e</h4>
<strong> Terakhir, Conan harus membuat file list.txt di folder “/home/[USER]/modul2/air” dan membuat list nama semua hewan yang ada di directory “/home/[USER]/modul2/air” ke “list.txt” dengan format UID_[UID file permission]_Nama File.[jpg/png] dimana UID adalah user dari file tersebut. file permission adalah permission dari file tersebut.
Contoh : conan_rwx_hewan.png </strong>
<br><br>
Kode untuk membuat file list.txt ialah :

```
FILE * list;
    list = fopen ("/home/nadya/modul2/air/list.txt", "w+");
```

Untuk menulis user, user's permissions, dan nama file adalah :

```
	    struct stat info;
	    int r;
	    r = stat("/home/nadya/modul2/darat", &info);
	    if(r == -1){
	      fprintf(stderr, "File error\n");
	      exit(1);
	    }
	    struct passwd *pw = getpwuid(info.st_uid);
	    if(pw != 0) fprintf(list, "%s_" , pw->pw_name);
	    struct stat fs;
	    if( fs.st_mode & S_IRUSR )
	      fprintf(list, "r");
	    if( fs.st_mode & S_IWUSR )
	      fprintf(list, "w");
	    if( fs.st_mode & S_IXUSR )
	      fprintf(list, "x");


	    fprintf(list, "_%s\n", dir12->d_name);
```

Dalam file "list.txt", terlihat seperti berikut :
<img src="foto/soal3/3e1.png" alt="3e1">
<img src="foto/soal3/3e2.png" alt="3e2">

<h2>Kendala</h2>
Dalam pengerjaan soal-soal di atas ditemui beberapa kendala yang akan dijabarkan berdasarkan soal. Berikut kendala-kendala yang ditemui:<br>
<h5>Kendala Soal 1 : </h5>
<ol type="i">
	<li>Kesusahan ketika menggunakan wget karena beberapa kali gagal</li>
	<li>Lupa melakukan fork</li>
    <li>Belum familiar terhadap json</li>
    <li>Kesalahan menulis syntax</li>
</ol>

<h5>Kendala Soal 2 : </h5>
<ol type="i">
	<li>Terkendala dengan fork()</li>
	<li>Kesulitan memanggil fungsi</li>
    <li>Bingung executenya program urutan ketika menggunakan fork()</li>
    <li>Kesusahan saat membuat daftar nama di data.txt</li>	
    <li>Kesusahan saat memisahkan dengan menduplikat dile yang dalam satu foto terdapat dua folder</li>	
	<li>Berkali-kali error karena salah pemilihan syntax</li>
</ol>

<h5>Kendala Soal 3 : </h5>
<ol type="i">
	<li>Terkendala ketika lupa fork()</li>
	<li>Penggunaan wait(&status)</li>
	<li>Kesulitan ketika penggunaan fungsi exec() yakni execv, execl, dsb.</li>
	<li>Kesusahan saat membuat daftar nama di list.txt</li>	
	<li>Berkali-kali error karena salah pemilihan syntax</li>	
</ol>
